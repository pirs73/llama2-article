from langchain.memory import ConversationBufferMemory
from langchain_community.llms import CTransformers
from langchain.chains import LLMChain
from langchain.prompts import PromptTemplate
import bs4
import streamlit as st
from langchain_core.prompts import SystemMessagePromptTemplate
from streamlit_chat import message
import tempfile
from langchain_community.embeddings import HuggingFaceEmbeddings
from langchain_community.vectorstores import FAISS
from langchain.chains import ConversationalRetrievalChain
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain_community.document_loaders import WebBaseLoader
from langchain.chains.question_answering import load_qa_chain
import os
# from docx import Document
# from docx.shared import Inches
import io
from tavily import TavilyClient

from PIL import Image
import requests

# load the model
MODEL_PATH = "../llama.cpp/models/13B/llama-2-13b-chat.Q4_0.gguf"
DB_FAISS_PATH = "vectorstore/db_faiss"

template = """
  You are a cryptocurrenty and SEO expert and your job is to write a news review {question}. The review should be 
  no more than 800 words.

  CONTEXT:
  {context}

  QUESTION: 
  {query}

  CHAT HISTORY: 
  {chat_history}

  ANSWER:
  """


prompt_s = PromptTemplate(input_variables=["chat_history", "query", "context"], template=template)
memory = ConversationBufferMemory(memory_key="chat_history", input_key="query")


def load_llm(max_tokens, prompt, retriever) -> CTransformers:
    """Load Llama model"""
    # Make sure the model path is correct for your system!
    llm: CTransformers = CTransformers(
        model=MODEL_PATH,
        model_type="llama",
        temperature=0,
        max_new_tokens=max_tokens,
        # lib='avx',
    )
    # llm_chain = LLMChain(
    #     llm=llm,
    #     prompt=PromptTemplate.from_template(prompt_template)
    # )

    llm_chain = ConversationalRetrievalChain.from_llm(
        llm=llm,
        retriever=retriever,
        verbose=False,
        condense_question_llm=llm,
        chain_type="stuff",
        get_chat_history=lambda h: h,
        memory=memory,
        combine_docs_chain_kwargs={"prompt": prompt}
    )

    return llm_chain


key_file = './tavilyapi.txt'

with open(key_file, 'r') as f:
    TAVILY_API_KEY: str = f.read().strip()

tavily = TavilyClient(api_key=TAVILY_API_KEY)


def fetch_query(query):
    print(query)
    response = tavily.search(query, search_depth="advanced", include_images=False)
    # context = tavily.get_search_context(query, search_depth="advanced", include_images=False, max_tokens=2000)
    # print(context)
    return response['results'][0]['url']
    # return context


# def create_word_docx(user_input, paragraph, image_input):
#     doc = Document()
#
#     doc.add_heading(user_input, level=1)
#     doc.add_paragraph(paragraph)
#
#     doc.add_heading('Image', level=1)
#     image_stream = io.BytesIO()
#     image_input.save(image_stream, format='PNG')
#     image_input.seek(0)
#     doc.add_picture(image_stream, width=Inches(4))
#
#     return doc


st.set_page_config(layout="wide")


def main():
    st.title("Article Generator app using Llama 2")
    st.markdown("<h3 style='text-align: center; color: white;'>Built by "
                "<a href='https://gitlab.com/pirs73/llama2-article'>AI Llama 2 "
                "with ❤️ </a></h3>", unsafe_allow_html=True)
    user_input = st.text_input("Please enter your topic or idea for article generator.")
    # image_input = st.text_input("Please enter your topic or idea for image.")

    if len(user_input) > 0:
        # col1 = st.columns([1])
        url = fetch_query(user_input)
        loader = WebBaseLoader(url)
        docs = loader.load()
        len(docs)
        text_splitter = RecursiveCharacterTextSplitter(
            chunk_size=3000,
            chunk_overlap=1000,
            length_function=len,
            is_separator_regex=False
        )
        splits = text_splitter.split_text(str(docs))
        data = text_splitter.create_documents(splits)

        embeddings = HuggingFaceEmbeddings(model_name='sentence-transformers/all-MiniLM-L6-v2',
                                           model_kwargs={'device': 'cpu'})
        db = FAISS.from_documents(data, embeddings)
        db.save_local(DB_FAISS_PATH)

        st.subheader("Generated Content by Llama 2")
        st.write("Your submitted idea/topic for article generation: " + user_input)

        prompt_template = """You are a digital marketing and SEO expert and your task is to write article so 
        write an article on the given topic. The article must be under 800 words. The article should be be lengthy.
        """
        retriever = db.as_retriever(search_kwargs={"k": 3})
        # llm_call = load_llm(max_tokens=2000, prompt_template=prompt_template, retriever=retriever)
        llm_call = load_llm(max_tokens=2000, prompt=prompt_s, retriever=retriever)

        result = llm_call({'question': user_input, 'query': user_input})
        if len(result) > 0:
            st.info("Your article has been been generated successfully!")
            st.write(result)
        else:
            st.error("Your article couldn't be generated!")


if __name__ == "__main__":
    main()

