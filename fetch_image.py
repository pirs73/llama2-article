import requests
from tavily import TavilyClient


key_file = './tavilyapi.txt'

with open(key_file, 'r') as f:
  TAVILY_API_KEY: str = f.read().strip()


tavily = TavilyClient(api_key=TAVILY_API_KEY)


def fetch_photo(query):
    response = tavily.search(query, search_depth="advanced", include_images=True)
    # context = tavily.get_search_context(query, search_depth="advanced", include_images=True, max_tokens=2000)
    photos = response['images']

    if photos:
        src_original_url = photos[0]
        return src_original_url
    else:
        print("No photos found")

    return None


# example usage of this function
query1 = "Bitcoin"

src_original_url = fetch_photo(query1)

if src_original_url:
    print(f"Original URL for '{query1}': {src_original_url}")

