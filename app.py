from langchain_community.llms import CTransformers
from langchain.chains import LLMChain
from langchain.prompts import PromptTemplate

import streamlit as st
import os
from docx import Document
from docx.shared import Inches
import io
from tavily import TavilyClient

from PIL import Image
import requests

# load the model
MODEL_PATH = "../llama2-cpu/llama-2-13b-chat.Q4_0.gguf"


def load_llm(max_tokens, prompt_template) -> CTransformers:
    """Load Llama model"""
    # Make sure the model path is correct for your system!
    llm: CTransformers = CTransformers(
        model=MODEL_PATH,
        model_type="llama",
        temperature=0.5,
        max_new_tokens=max_tokens,
        lib='avx',
    )
    llm_chain = LLMChain(
        llm=llm,
        prompt=PromptTemplate.from_template(prompt_template)
    )

    return llm_chain


key_file = './tavilyapi.txt'

with open(key_file, 'r') as f:
    TAVILY_API_KEY: str = f.read().strip()

tavily = TavilyClient(api_key=TAVILY_API_KEY)


def fetch_photo(query):
    response = tavily.search(query, search_depth="advanced", include_images=True)
    # TODO: for RAG
    # context = tavily.get_search_context(query, search_depth="advanced", include_images=True, max_tokens=2000)
    photos = response['images']

    if photos:
        src_original_url = photos[0]
        return src_original_url
    else:
        print("No photos found")

    return None


def create_word_docx(user_input, paragraph, image_input):
    doc = Document()

    doc.add_heading(user_input, level=1)
    doc.add_paragraph(paragraph)

    doc.add_heading('Image', level=1)
    image_stream = io.BytesIO()
    image_input.save(image_stream, format='PNG')
    image_input.seek(0)
    doc.add_picture(image_stream, width=Inches(4))

    return doc


st.set_page_config(layout="wide")


def main():
    st.title("Article Generator app using Llama 2")
    user_input = st.text_input("Please enter your topic or idea for article generator.")
    image_input = st.text_input("Please enter your topic or idea for image.")

    if len(user_input) and len(image_input) > 0:
        col1, col2, col3 = st.columns([1, 2, 1])

        with col1:
            st.subheader("Generated Content by Llama 2")
            st.write("Your submitted idea/topic for article generation: " + user_input)
            st.write("Your submitted idea/topic for image: " + image_input)

            prompt_template = """You are a digital marketing and SEO expert and your task is to write article so 
            write an article on the given topic: {user_input}. The article must be under 800 words. 
            The article should be be lengthy."""
            llm_call = load_llm(max_tokens=800, prompt_template=prompt_template)
            print(llm_call)
            result = llm_call(user_input)
            if len(result) > 0:
                st.info("Your article has been been generated successfully!")
                st.write(result)
            else:
                st.error("Your article couldn't be generated!")

        with col2:
            st.subheader("Fetched Image")
            image_url = fetch_photo(image_input)
            st.image(image_url)

        with col3:
            st.subheader("Final Article to Download")
            # image_input = "temp_image.jpg"
            image_response = requests.get(image_url)
            img = Image.open(io.BytesIO(image_response.content))
            doc = create_word_docx(user_input, result['text'], img)

            # Save the Word document to a BytesIO buffer
            doc_buffer = io.BytesIO()
            doc.save(doc_buffer)
            doc_buffer.seek(0)

            # Prepare the download link
            st.download_button(
                label='Download Word Document',
                data=doc_buffer,
                file_name='document.docx',
                mime='application/vnd.openxmlformats-officedocument.wordprocessingml.document'
            )


if __name__ == "__main__":
    main()
