import requests
import json
from tavily import TavilyClient

key_file = './tavilyapi.txt'

with open(key_file, 'r') as f:
  TAVILY_API_KEY: str = f.read().strip()


tavily = TavilyClient(api_key=TAVILY_API_KEY)


def fetch_query(query):
    response = tavily.search(query, search_depth="advanced", include_images=False)
    # context = tavily.get_search_context(query, search_depth="advanced", include_images=True, max_tokens=2000)
    print(response['results'][0]['content'])
    # print(response)

    # if photos:
    #     src_original_url = photos[0]
    #     return src_original_url
    # else:
    #     print("No photos found")

    return None


# example usage of this function
query1 = "news about Bitcoin"

fetch_query(query1)